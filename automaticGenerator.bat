@echo off
setlocal enabledelayedexpansion

echo 納品物作成を開始します

rem バッチが置かれている場所をカレントディレクトリに設定
cd /d %~dp0

rem 納品物一式フォルダが存在する場合は削除
if exist 納品物一式 (
    rmdir /s /q 納品物一式
)

rem 必要ディレクトリを作成 tempは作業用フォルダ
rem TODO 生成した納品物フォルダ自体を納品ブランチにコミットしてpushすれば納品物もgit管理できるかもしれない(ver2.0)
rem 日本語でも大丈夫そうなものは一旦日本語のままにしておく
mkdir delivs
cd delivs
mkdir temp sourceCodes ソースコード差分 modules 設計書 試験項目書 テスト工程完了チェックシート

rem 1リポジトリ毎にソースコード差分、ソースコード一式、実行モジュール作成を行う
for /f "delims=, tokens=1,2,3,4 skip=1" %%a in (..\config\repositoryURL.csv) do (
    cd %~dp0\delivs\temp
    
    rem リポジトリ名, プロジェクトリポジトリURL, 最新納品ブランチ名, 前回納品ブランチ名を取得
    set name=%%a
    set url=%%b
    set latestBranch=%%c
    set previousBranch=%%d
    rem 前回納品時のソースコード(比較用)フォルダ用にディレクトリの名前を設定
    set previous=!name!_前回

    rem リポジトリcloneバッチを実行　tempにリポジトリをcloneする　repositoryURL.csvからリポジトリのurlを取得してgit cloneコマンドを実行する 比較用に2つ生成される
    call %~dp0\child_batch\cloneRepositories.bat !name! !url! !latestBranch! !previousBranch! !previous!
    
    rem 差分取得バッチを実行
    call %~dp0\child_batch\compareDifferences.bat !name! !previous!
    
    rem ソースコード一式(zip)生成バッチを実行
    call %~dp0\child_batch\compressSourceCodes.bat !name!

    rem 実行モジュールのビルドバッチを実行
    call %~dp0\child_batch\buildModules.bat !name!
    rem TODO バッチの実行結果を戻り値で受け取って処理の制御した方がいいか？
)

rem tempに移動
cd %~dp0\delivs\temp

rem 設計書出力バッチを実行
call %~dp0\child_batch\moveDocuments.bat

rem tf-deliverables-automatic-generator/delivsに移動
cd %~dp0\delivs
rem フォルダ名をリネーム
ren modules 実行モジュール
ren sourceCodes ソースコード一式

rem tf-deliverables-automatic-generator/に移動
cd %~dp0

rem tempフォルダは最後に消す
rd /s /q delivs\temp

rem 最後にフォルダ名をリネーム
ren delivs 納品物一式

rem 最後にpauseする
echo 納品物の作成が完了しました。問題ないか必ず確認した上で納品してください
pause