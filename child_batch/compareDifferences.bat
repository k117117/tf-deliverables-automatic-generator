@echo off
setlocal enabledelayedexpansion

echo 差分ファイル生成を開始します

set name=%1
set previous=%2

rem 差分を取る　WinMergeをCLIから実行する場合差分のあるディレクトリのみ展開してレポートを生成することはできなさそうなのでツリービューは全て展開される
rem TODO 差分時の和名_管理web、みたいな_も設定ファイルで取り込めるようにするといいかも
rem tf-deliverables-automatic-generator/に移動
cd %~dp0\..
mkdir delivs\ソースコード差分\!name!
call tools\WinMerge\WinMergeU.exe /r /wl /wr /minimize /noninteractive delivs\temp\!previous! delivs\temp\!name! /or delivs\ソースコード差分\!name!\ソースコード差分.html