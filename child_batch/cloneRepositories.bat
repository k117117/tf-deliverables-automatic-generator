@echo off
setlocal enabledelayedexpansion

echo リポジトリのcloneを開始します

rem リポジトリ名, プロジェクトリポジトリURL, 最新納品ブランチ名, 前回納品ブランチ名を取得
rem NOTE バッチの実行時引数は1始まりらしい。笑
set name=%1
set url=%2
set latestBranch=%3
set previousBranch=%4
rem 前回納品時のソースコード(比較用)フォルダ用にディレクトリの名前を設定
set previous=%5

rem カレントディレクトリをtf-deliverables-automatic-generator\delivs\tempに設定
cd %~dp0\..\delivs\temp
rem 最新ブランチ用のリポジトリをclone
git clone !url! !name!
rem 前回ブランチ用のリポジトリをclone
git clone !url! !previous!

rem 差分比較を行うブランチに切り替える_最新ブランチ(は作成してある前提)
cd !name!
git checkout -b !latestBranch! origin/!latestBranch!
rem git関連ファイルを削除
rmdir /s /q .git
del .gitignore

rem 差分比較を行うブランチに切り替える_前回ブランチ
cd ..\!previous!
git checkout -b !previousBranch! origin/!previousBranch!
rem git関連ファイルを削除
rmdir /s /q .git
del .gitignore

rem tf-deliverables-automatic-generator\に移動
cd %~dp0\..