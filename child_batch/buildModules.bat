@echo off
setlocal enabledelayedexpansion

echo 実行モジュールのビルドを開始します

set name=%1

rem プロジェクトルートに移動
cd %~dp0\..\delivs\temp\!name!
rem プロジェクトルートにslnファイルがないものもあるのでサブディレクトリまで再帰的に検索してslnファイルのパスを取得
for /f "usebackq" %%t in (`dir /b /s /a-d *.sln`) do set sln_path=%%t
rem slnファイルがない場合は処理を終了
if not exist !sln_path! (
   echo slnファイルが存在しません。リポジトリの状態を確認してください：!name!
   exit /b
)
rem nugetで管理下のパッケージを復元　3.4.4以外のバージョンを使いたい場合は別途インストールしてください
call %~dp0\..\tools\nuget\3.4.4\nuget.exe restore !sln_path!

rem NOTE VS2013のMSBuildとかだとC#6以降の構文で書かれたコードがコンパイルエラーになる　なのでVS2015以降のMSBuildを使用すること
rem buildTargetProjects.csvからビルド対象のプロジェクトと設定値を取得してビルドする
for /f "delims=, tokens=1,2,3,4,5 skip=1" %%a in (..\..\..\config\buildTargetProjects.csv) do (
    if %%a equ !name! (
        rem ビルド対象プロジェクト名、ビルド構成、ビルド対象プラットフォームを取得
        set buildTarget=%%b
        set buildMethod=%%c
        set conf=%%d
        set platform=%%e
        rem TODO slnまるごとビルド、つまり /t:rebuild の場合はどうしようかな(ver2.0) slnだったら...みたいにすればいんじゃないか(ver2.0)
        rem TODO Web発行への対応はver2.0で行う　なおweb発行をする場合はpublish profileが必要なんだけどそれはtf-deliverables-automatic-generatorフォルダに、案件ごとに作っておけばいいはず
        rem NOTE OutputPathオプションで出力先を指定してビルドすると上手く行かないことがあるのでbinに出力した後にディレクトリ移動する方式を取っている
        "C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\MSBuild.exe" "!sln_path!"  /t:!buildTarget!:!buildMethod! /p:Configuration=!conf!;Platform=!platform!
        
        rem 旧環境用
        rem "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe" "!sln_path!"  /t:!buildTarget!:!buildMethod! /p:Configuration=!conf!;Platform=!platform!
        
        rem 新環境用
        rem "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild.exe" "!sln_path!"  /t:!buildTarget!:!buildMethod! /p:Configuration=!conf!;Platform=!platform!

        rem ビルドに失敗した場合delivsフォルダを削除してやり直しを促す
        if !ERRORLEVEL! neq 0 (
            echo "ビルドに失敗しました。リポジトリを確認してやり直してください。なおdelivsフォルダは削除されます ErrorLevel:!ERRORLEVEL!"
            pause
            cd %~dp0\..
            rd /s /q delivs
            exit /b
        )
        
        rem ビルドしたモジュールのルートフォルダをmodulesフォルダに移す
        rem NOTE 各projectでビルド時に出力パスが設定されている場合はbinに出力するように変更が必要(fastのバッチとかそうだった)
        mkdir ..\..\modules\!name!
        move !buildTarget! ..\..\modules\!name!\
    )
)

rem tf-deliverables-automatic-generator/に移動
cd %~dp0\..