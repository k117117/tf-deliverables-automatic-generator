@echo off
setlocal enabledelayedexpansion

echo "ソースコード一式(zip)作成を開始します"

set name=%1

rem ソースコード一式のzip化
rem TODO ソースコード一式の和名_管理web.zip、みたいな_も設定ファイルで取り込めるようにするといいかも
rem tf-deliverables-automatic-generator/に移動
cd %~dp0\..
call tools\7za.exe a .\delivs\sourceCodes\!name!.zip .\delivs\temp\!name!