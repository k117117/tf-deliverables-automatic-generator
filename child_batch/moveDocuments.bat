@echo off
setlocal enabledelayedexpansion

echo "納品対象設計書の出力を開始します"

rem tempに移動
cd %~dp0\..\delivs\temp

rem 設計書がgit管理されている場合は納品対象の設計書を設計書フォルダに移す
for /f "delims=, tokens=1,2,3,4" %%a in (..\..\config\docURL.csv) do (
    rem 設計書がgit管理されている場合
    if %%b equ Yes (
        echo 納品対象設計書の出力を開始します
        set url=%%d
        git clone !url! doc
        cd doc
    ) else if %%b equ No (
        rem 設計書がgit管理されていない場合
        goto noDoc
    )

    rem TODO 設計書がサブディレクトリ分けされて管理されてる場合の対応もしないといけないかもしれんがver2.0でその機能は追加する・・・？
    move %%b ..\..\設計書
)

:noDoc

rem tf-deliverables-automatic-generator/に移動
cd %~dp0\..